package u04lab.code

import Optionals._
import Lists._
import Lists.List._
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class CourseTest {

  @Test def testCourses(): Unit ={
    val cPPS = Course("PPS","Viroli")
    val cPCD = Course("PCD","Ricci")
    val cSDR = Course("SDR","D'Angelo")
    val s1 = Student("mario",2015)
    val s2 = Student("gino",2016)
    val s3 = Student("rino") //defaults to 2017
    s1.enrolling(cPPS)
    s1.enrolling(cPCD)
    s2.enrolling(cPPS)
    s3.enrolling(cPPS, cPCD, cSDR)
    assertEquals(s1.courses, Cons("PCD",Cons("PPS",Nil())))
    assertEquals(s2.courses, Cons("PPS",Nil()))
    assertEquals(s3.courses, Cons("SDR",Cons("PCD",Cons("PPS",Nil()))))
  }

  @Test def testHasTeacher(): Unit ={
    val cPPS = Course("PPS","Viroli")
    val cPCD = Course("PCD","Ricci")
    val cSDR = Course("SDR","D'Angelo")
    val s1 = Student("mario",2015)
    val s2 = Student("gino",2016)
    val s3 = Student("rino") //defaults to 2017
    s1.enrolling(cPPS)
    s1.enrolling(cPCD)
    s2.enrolling(cPPS)
    s3.enrolling(cPPS)
    s3.enrolling(cPCD)
    s3.enrolling(cSDR)
    assertTrue(s1.hasTeacher("Ricci"))
    assertTrue(s2.hasTeacher("Viroli"))
    assertTrue(s3.hasTeacher("D'Angelo"))
  }

}
