package u04lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u04lab.code.Lists._
import u04lab.code.Lists.List._

class ListTest {

  @Test def testFactory(): Unit ={
    val lst = List(1, 2, 3, 4, 5)
    assertEquals(Cons(1, Cons(2, Cons(3, Cons(4, Cons(5, nil))))), lst)
  }

}
