package u04lab.code

import Optionals._
import Lists._
import Lists.List._
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class ComplexTest {

  @Test def testSum(): Unit ={
    val x: Complex = new ComplexImpl(10, 20)
    val y: Complex = new ComplexImpl(1, 1)
    val sum: Complex = new ComplexImpl(11, 21)
    assertEquals(sum.re , (x + y).re)
    assertEquals(sum.im , (x + y).im)
    //assertEquals(sum, (x + y))
    //assertTrue(new ComplexImpl(11, 21) == (x + y))
  }

  @Test def testMul(): Unit ={
    val x = new ComplexImpl(10, 20)
    val y = new ComplexImpl(1, 1)
    assertEquals(new ComplexImpl(-10, 30).re , (x * y).re)
    assertEquals(new ComplexImpl(-10, 30).im , (x * y).im)
    //assertEquals(new ComplexImpl(200, 600) , (x * y))
    //assertTrue(new ComplexImpl(200, 600) == (x * y))
  }

}
