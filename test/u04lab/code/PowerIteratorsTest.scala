package u04lab.code

import Optionals._
import Lists._
import Lists.List._
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class PowerIteratorsTest {

  val factory = new PowerIteratorsFactoryImpl()

  @Test
  def testIncremental() {
    val pi = factory.incremental(5,_+2); // pi produce 5,7,9,11,13,...
    assertEquals(Option.of(5), pi.next());
    assertEquals(Option.of(7), pi.next());
    assertEquals(Option.of(9), pi.next());
    assertEquals(Option.of(11), pi.next());
    assertEquals(List.Cons(5, List.Cons(7, List.Cons(9, List.Cons(11,List.Nil())))), pi.allSoFar()); // elementi già prodotti
    for (i <- 0 until 10) {
      pi.next(); // procedo in avanti per un po'..
    }
    assertEquals(Option.of(33), pi.next()); // sono arrivato a 33
  }

  @Test
  def testFromList() {
    val lst : List[Int] = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
    val pi = factory.fromList(lst); // pi produce 5,7,9,11,13,...
    assertEquals(Option.of(3), pi.next());
    assertEquals(Option.of(7), pi.next());
    assertEquals(Option.of(1), pi.next());
    assertEquals(Option.of(5), pi.next());
    assertEquals(List.Cons(3, List.Cons(7, List.Cons(1, List.Cons(5,List.Nil())))), pi.allSoFar()); // elementi già prodotti

    assertEquals(Option.empty, pi.next()); // sono arrivato a 33
  }

  @Test
  def testRandomBooleans() {
    val pi = factory.randomBooleans(5); // pi produce 5,7,9,11,13,...
    pi.next()
    pi.next()
    pi.next()
    pi.next()
    assertEquals(4, length(pi.allSoFar()))
    pi.next()
    assertEquals(Option.empty, pi.next())
  }
}