package u04lab.code

import u04lab.code.Lists.List.{Cons, Nil, _}
import Optionals._
import u04lab.code.Streams._
import u04lab.code.Streams.Stream._
import u04lab.code.Lists._

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = new PowerIterator[Int] {

    var list: List[Int] = nil
    var actual: Int = start

    override def next(): Option[Int] = {
      val n = actual
      actual= successive(actual)
      list = append(list, List.Cons(n, nil) )
      Option.of(n)
    }

    override def allSoFar(): List[Int] = list

    override def reversed(): PowerIterator[Int] = fromList(reverse(list))
  }

  override def fromList[A](list: List[A]): PowerIterator[A] = new PowerIterator[A] {

    val lists: Lists.List[A] = list
    var toReturn = 0

    override def next(): Option[A] = drop(lists, toReturn) match {
      case Cons(h, t) => {
        toReturn = toReturn + 1
        Option.of(h)
      }
      case Nil() => Option.empty

    }

    override def allSoFar(): List[A] = {
      var i = 0
      var l : List[A] = nil
      while(i < toReturn) {
        drop(lists, i) match {
          case Cons(h, t) => l = append(l, Cons(h, nil))
          case Nil() => l
        }
        i = i + 1
      }
      l
    }

    override def reversed(): PowerIterator[A] = fromList(reverse(lists))
  }

  override def randomBooleans(size: Int): PowerIterator[Boolean] = new PowerIterator[Boolean] {

    var list: List[Boolean] = nil
    var nElem = 0
    val random = new Random()
    val stream : Stream[Boolean] = take(Stream.generate(random.nextBoolean()))(size)

    override def next(): Option[Boolean] = {
      if(nElem < size) {
        nElem = nElem + 1
        val b = random.nextBoolean()
        list = append(list, List.Cons(b, Nil()))
        Option.of(b)
      } else Option.empty
    }

    override def allSoFar(): List[Boolean] = list

    override def reversed(): PowerIterator[Boolean] = fromList(reverse(list))
  }

}


object Main extends App(){
  var el = new PowerIteratorsFactoryImpl().incremental(1, _+2)
  val lst: List[Int] = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
  var el2 = new PowerIteratorsFactoryImpl().fromList(lst)
  var el3 = new PowerIteratorsFactoryImpl().randomBooleans(5)
  println(length(lst))
  System.out.println(el.next()) //Some(1)
  System.out.println(el.next()) //Some(3)
  System.out.println(el.next()) //Some(5)
  System.out.println(el.allSoFar()) //Some(1, Some(3, Some(5, Nil())))
  val p = el.reversed()
  System.out.println(p.next())
  System.out.println(p.next())
  System.out.println(p.next())
  System.out.println("------------")


  System.out.println(el2.next()) //Some(3)
  System.out.println(el2.next()) //Some(7)
  System.out.println(el2.next()) //Some(1)
  System.out.println(el2.allSoFar()) //Some(3, Some(7, Some(1, Nil())))
  System.out.println("------------")

  System.out.println(el3.next())
  System.out.println(el3.next())
  System.out.println(el3.next())
  System.out.println(el3.next())
  System.out.println(el3.allSoFar())
  System.out.println(el3.next())
  System.out.println(el3.next())

}